package Exam;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Browser {

    public static WebDriver GetDriver()
    {
        System.setProperty("webdriver.gecko.driver","C:\\QA Dev\\geckodriver.exe");
        return new FirefoxDriver();
    }

    public static WebDriver driver = new ChromeDriver();

    public static void goTo(String url){
        driver.get(url);
    }
    public static String title() {
        return driver.getTitle();
    }
    public static void close() {
        driver.close();
    }
}
