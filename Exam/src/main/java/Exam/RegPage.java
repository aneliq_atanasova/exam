package Exam;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class RegPage {
    private WebDriver _driver;
    private String _successRegisterMessage = "Welcome to your account. Here you can manage all of your personal information and orders.";

    public RegPage(WebDriver driver) {
        _driver = driver;
    }

    public void SetPersonalInformation(String firstName, String lastName, String password, Boolean isMr, int day, int month, int year, Boolean isSignUpForNewsletter, Boolean IsReceiveOffers) {
        _driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation");

        if (isMr) {
            WebElement Radio1 = _driver.findElement(By.id("id_gender1"));
            Radio1.click();
        } else {
            WebElement Radio1 = _driver.findElement(By.id("id_gender2"));
            Radio1.click();
        }

        _driver.findElement(By.id("customer_firstname")).sendKeys(firstName);
        _driver.findElement(By.id("customer_lastname")).sendKeys(lastName);
        // _driver.findElement(By.id("email")).sendKeys(email);
        _driver.findElement(By.id("passwd")).sendKeys(password);

        WebElement days = _driver.findElement(By.id("days"));
        Select selectDay = new Select(days);
        selectDay.selectByValue(String.valueOf(day));

        WebElement month_dropdown = _driver.findElement(By.id("months"));
        Select selectMonth = new Select(month_dropdown);
        selectMonth.selectByValue(String.valueOf(month));

        WebElement years = _driver.findElement(By.id("years"));
        Select selectYear = new Select(years);
        selectYear.selectByValue(String.valueOf(year));

        if (isSignUpForNewsletter) {
            _driver.findElement(By.id("newsletter")).click();
        }
        if (IsReceiveOffers) {
            _driver.findElement(By.id("optin")).click();
        }

    }
    public void SetAddress(String firstName, String lastName, String company, String address, String add2, String city, String postcode, String text, String hphone, String mphone, String alias) {
        _driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation");
        _driver.findElement(By.id("firstname")).sendKeys(firstName);
        _driver.findElement(By.id("lastname")).sendKeys(lastName);
        _driver.findElement(By.id("company")).sendKeys(company);
        _driver.findElement(By.id("address1")).sendKeys(address);
        _driver.findElement(By.id("address2")).sendKeys(add2);
        _driver.findElement(By.id("city")).sendKeys(city);

        Select State = new Select(_driver.findElement(By.name("id_state")));
        State.selectByVisibleText("Delaware");

        _driver.findElement(By.id("postcode")).sendKeys(postcode);

        Select drpCountry = new Select(_driver.findElement(By.name("id_country")));
        drpCountry.selectByValue("21");

        _driver.findElement(By.name("other")).sendKeys(text);
        _driver.findElement(By.name("phone")).sendKeys(hphone);
        _driver.findElement(By.name("phone_mobile")).sendKeys(mphone);
        _driver.findElement(By.name("alias")).sendKeys(alias);
//        _driver.findElement(By.id("submitAccount")).click();
    }
    public Boolean SaveForm(){
        _driver.findElement(By.id("submitAccount")).click();

        List<WebElement> listOfElements = _driver.findElements(By.cssSelector("#center_column > p"));

        String loginInfoMessage = "";
        if (listOfElements.size() > 0)
        {
            loginInfoMessage = listOfElements.get(0).getText();
        }

        if ( _successRegisterMessage.equals(loginInfoMessage))
        {
            return true;
        }

        return false;
    }

    public String ErrorMessage() {
        WebElement dangerAlertWebElement = _driver.findElement(By.cssSelector("#center_column .alert-danger"));
        String dangerAlertText = dangerAlertWebElement.getText();
        return dangerAlertText;
    }

    public String GetFirstRequiredElement() {
        WebElement ErrorZ = _driver.findElement(By.cssSelector("#center_column > div > ol > li"));
        String ErrorZi = ErrorZ.getText();
        return ErrorZi;
    }

    public List<String> GetAllRequiredEmptyFields()
    {
        List<WebElement> listOfRequiredEmptyFields = _driver.findElements(By.cssSelector("#center_column > div > ol > li"));

        List<String> emptyFields =  new ArrayList<String>();

        for (int i = 0; i < listOfRequiredEmptyFields.size(); i++) {

            WebElement li = listOfRequiredEmptyFields.get(i);
            String requredFieldName = li.findElement(By.tagName("b")).getText();
            emptyFields.add(requredFieldName);
        }

        return emptyFields;
    }

}


