package Exam;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AuthenticationPage {
    private String _successRegisterMessage = "Welcome to your account. Here you can manage all of your personal information and orders.";
    private WebDriver _driver;

    public AuthenticationPage(WebDriver driver){
        _driver=driver;
    }

    public void CreateAnAcc(String email) throws InterruptedException {
         _driver.findElement(By.id("email_create")).sendKeys(email);
         _driver.findElement(By.cssSelector("#SubmitCreate > span")).click();
    }

    public String GetErrorMessageCreateAnAcc()
    {
        WebElement wrongMessageElement = _driver.findElement(By.cssSelector("#create_account_error"));
        String textWrongEmail = wrongMessageElement.getText();
        return  textWrongEmail;
    }

    public Boolean LoginIn(String userName,String password){
        _driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation");

        _driver.findElement(By.id("email")).sendKeys(userName);
     //   _userName.sendKeys(userName);

        _driver.findElement(By.id("passwd")).sendKeys(password);
        _driver.findElement(By.id("SubmitLogin")).click();

        List<WebElement> elements = _driver.findElements(By.cssSelector("#center_column > p"));

        String loginInfoMessage = "";
        if (elements.size() > 0) {
            loginInfoMessage = elements.get(0).getText();
        }

        if ( _successRegisterMessage.equals(loginInfoMessage))
        {
            return true;
        }

        return false;
    }

    public void SingOut(){
        _driver.findElement(By.id("my-account")).click();
    }

    public String ErrorMessage() {
        WebElement dangerAlertWebElement = _driver.findElement(By.cssSelector("#center_column .alert-danger"));
        String dangerAlertText = dangerAlertWebElement.getText();
        return dangerAlertText;
    }

    public String ErrorEmailMessage(){
        WebElement fail = _driver.findElement(By.cssSelector("#center_column > div.alert.alert-danger > ol > li"));
        String faillog = fail.getText();
        return faillog;
    }

    public String ErrorPass(){
        WebElement Error = _driver.findElement(By.id("center_column"));
        String ErrorP = Error.getText();
        return ErrorP;
    }



    /*
    @FindBy(how = How.ID, using = "email")
    private WebElement _userName;

    // Messages
    @FindBy(how = How.ID, using = "center_column")
    private WebElement _messageOut;
    */
}

// Елементи
// input, textarea  -> текстови полете
// <a href -> ликове и бутони
// button, submit  -> бутони
// select  -> падащо меню
// input може да бъде различни видове: type="checkbox" , ...
// ----------------------------- всички останали: div, span, li, ... са тагове --> от тях може да се чете(променя) текс

