package Exam;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    static String _url = "http://automationpractice.com/index.php";
    static String title = "My Store";
    private WebDriver _driver;

    public HomePage(WebDriver driver) {
        _driver = driver;
    }

    public void SignIn() {
        _driver.get(_url);
        _driver.findElement(new By.ByXPath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).click();
    }


    public boolean isAtSignInPage() {
        return Browser.title().equals(title);
    }

}

