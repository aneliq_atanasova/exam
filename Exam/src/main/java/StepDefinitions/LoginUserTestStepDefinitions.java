package StepDefinitions;

import Exam.AuthenticationPage;
import Exam.Browser;
import Exam.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;


public class LoginUserTestStepDefinitions {

    private WebDriver _browser;
    HomePage homePage;

    @Given("I have a browser")
    public void i_have_browser(){
        System.out.println("Step 1: RUN Browser");

        _browser = Browser.driver;
        _browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        homePage = new HomePage(_browser);
        System.out.println("Step 1: Done");
    }

    @When("I go to sign page")
    public void I_go_to_sign_page() throws InterruptedException {
        System.out.println("step 2: Sing page");
        homePage.SignIn();
        Thread.sleep(2000);
        System.out.println("step 2: Done");
    }

    @Then("try {string} with pass {string}")
    public  void Result(String name, String pass) throws InterruptedException {
        System.out.println("Step 3: Try Login with Email" + name + ", Password: " + pass);

        AuthenticationPage log = new AuthenticationPage(_browser);
        Boolean isSuccessLogin = log.LoginIn(name, pass);
        Thread.sleep(2000);

        log.SingOut();
        Thread.sleep(2000);

        System.out.println("Step 3: done");
        Assert.assertTrue(isSuccessLogin);
    }

}
