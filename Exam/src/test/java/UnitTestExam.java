
import Exam.AuthenticationPage;
import Exam.Browser;
import Exam.HomePage;
import Exam.RegPage;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class UnitTestExam {
    private String userName = "hak@gmail.com";
    private String password = "Qmwnebrv";

    @Test //т.1
    public void Register() throws InterruptedException {
        WebDriver browser = Browser.driver;

        HomePage homePage = new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage auto = new AuthenticationPage(browser);
        auto.CreateAnAcc("alab@gmjigfglz.com");
        Thread.sleep(2000);

        RegPage registerPage = new RegPage(browser);
        registerPage.SetPersonalInformation("Ivan", "Ivanopv", "kdhtys@589", true, 17, 6, 1992, true, true);
        registerPage.SetAddress("Ivan", "Ivanov", "MyCompany", "Nqkyde si", "ul.Nevsha", "NEVSHA", "90000", "", "+35988888888", "+359888565874", "alias123");
        Boolean isSuccessRegister =  registerPage.SaveForm();

        Assert.assertTrue(isSuccessRegister);
    }

    @Test //т.2
    public void checkForInvalidEmail() throws InterruptedException {
        String email ="kfsdjfk lls";
        WebDriver browser = Browser.driver;

        HomePage homePage = new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage auto = new AuthenticationPage(browser);
        auto.CreateAnAcc(email);

        Thread.sleep(2000);
        String textWrongEmail = auto.GetErrorMessageCreateAnAcc();
        String wrongEmailMessage = "Invalid email address.";
        Assert.assertEquals(wrongEmailMessage, textWrongEmail);
    }
    @Test //т.3.1
    public void Register_ErPassword() throws InterruptedException {
        WebDriver browser = Browser.driver;

        HomePage homePage = new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage auto = new AuthenticationPage(browser);
        auto.CreateAnAcc("ala321@gmjil.com");
        Thread.sleep(2000);

        RegPage registerPage = new RegPage(browser);
        registerPage.SetPersonalInformation("Ivan", "Ivanopv", "", true, 17, 6, 1992, true, true);
        registerPage.SetAddress("Ivan", "Ivanov", "MyCompany", "Nqkyde si", "", "NEVSHA", "90000", "", "+35988888888", "+359888565874", "alias123");
        registerPage.SaveForm();
        Thread.sleep(2000);

        String ErPas=registerPage.GetFirstRequiredElement();
        String pas= "passwd is required.";
        Assert.assertEquals(pas,ErPas);
    }
    @Test //т.3.2
    public void Register_ErZip() throws InterruptedException {
        WebDriver browser = Browser.driver;

        HomePage homePage = new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage auto = new AuthenticationPage(browser);
        auto.CreateAnAcc("alab12@gmjil.com");
        Thread.sleep(2000);

        RegPage registerPage = new RegPage(browser);
        registerPage.SetPersonalInformation("Ivan", "Ivanopv", "kdhtys@589", true, 17, 6, 1992, true, true);
        registerPage.SetAddress("Ivan", "Ivanov", "MyCompany", "Nqkyde si", "", "NEVSHA", "", "", "+35988888888", "+359888565874", "alias123");
        registerPage.SaveForm();
        Thread.sleep(2000);

        String ErZip=registerPage.GetFirstRequiredElement();
        String zip= "The Zip/Postal code you've entered is invalid. It must follow this format: 00000";
        Assert.assertEquals(zip,ErZip);
    }
    @Test //т.3.3
    public void Register_ErAddress() throws InterruptedException {
        WebDriver browser = Browser.driver;

        HomePage homePage = new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage auto = new AuthenticationPage(browser);
        auto.CreateAnAcc("alab1@gmji.com");
        Thread.sleep(2000);

        RegPage registerPage = new RegPage(browser);
        registerPage.SetPersonalInformation("Ivan", "Ivanopv", "kdhtys@589", true, 17, 6, 1992, true, true);
        registerPage.SetAddress("Ivan", "Ivanov", "MyCompany", "", "", "NEVSHA", "00000", "", "+35988888888", "+359888565874", "alias123");
        registerPage.SaveForm();
        Thread.sleep(2000);

        String ErAdd = registerPage.GetFirstRequiredElement();
        String address= "address1 is required.";
        Assert.assertEquals(address,ErAdd);
    }
    @Test //т.3.4
    public void FindAllRequiredEmptyFields() throws InterruptedException {
        WebDriver browser = Browser.driver;

        HomePage homePage = new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage auto = new AuthenticationPage(browser);
        auto.CreateAnAcc("ala3121@gmjil.com");
        Thread.sleep(2000);

        RegPage registerPage = new RegPage(browser);
        registerPage.SetPersonalInformation("Ivan", "", "", true, 17, 6, 1992, true, true);
        registerPage.SetAddress("Ivan", "", "MyCompany", "Nqkyde si", "", "NEVSHA", "90000", "", "+35988888888", "+359888565874", "alias123");
        registerPage.SaveForm();
        Thread.sleep(2000);


        List<String> listOfRequiredFields = registerPage.GetAllRequiredEmptyFields();

        int countError = listOfRequiredFields.size(); // count Errors

        String error = "";
        for (int i = 0; i < listOfRequiredFields.size(); i++) {
            error += "Error " + (i+1) + ": " + listOfRequiredFields.get(i) + "\n";
        }

        Assert.assertTrue("\n\nErrors "+ countError + " Required fields are: \n" + error, false);
    }
    @Test //т.4
    public void SuccessLogin() throws InterruptedException {
        String name = "hak@gmail.com";
        String pass = "Qmwnebrv";
        WebDriver browser = Browser.driver;

        HomePage homePage = new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage log = new AuthenticationPage(browser);
        Boolean isSuccessLogin = log.LoginIn(name, pass);

        Assert.assertTrue(isSuccessLogin);
    }
    @Test //т.5.1
    public void CanNotLogin() throws InterruptedException {
        String name = "hakdf";
        String pass = "Qmwnebrv";

        TyrToLogin(name, pass, "Invalid email address.");
    }
    @Test //т.5.2
    public void WrongPassword() throws InterruptedException {
        String email = "anel@yahoo.com";
        String password = "@@@@";

        TyrToLogin(email, password, "Invalid password.");
    }

    private void TyrToLogin(String userName, String password, String equalErrorMessage) throws InterruptedException {
        WebDriver browser = Browser.driver;

        HomePage homePage =  new HomePage(browser);
        homePage.SignIn();
        Thread.sleep(2000);

        AuthenticationPage log = new AuthenticationPage(browser);
        log.LoginIn(userName, password);
        Thread.sleep(2000);

        String wrongEmail = log.ErrorEmailMessage();
        Assert.assertEquals(equalErrorMessage, wrongEmail);
    }
}
